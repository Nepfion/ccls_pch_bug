cmake_minimum_required(VERSION 3.10)

project(test_project)
add_executable(test_exe main.cpp)
#target_compile_options(test_exe PRIVATE -include ${CMAKE_SOURCE_DIR}/include_me.h)
target_precompile_headers(test_exe PRIVATE 
    <algorithm>
    <memory>
    <vector>
    <unordered_map>
    <array>
    <fstream>
    <sstream>
    <iostream>
    <functional>
    <cstdint>
    <cassert>
    <chrono>
)

